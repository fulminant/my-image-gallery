import { Helpers } from './helpers';
import { IGalleryOptions } from '../interfaces/gallery.interface';
import { Swipe } from './swipes';

const helpers = new Helpers();

export class Gallery {
  public galleryID: string;
  public options: IGalleryOptions;
  private totalWidth: number = 0;
  private currentSlide: any;
  private currentSlideValue: number = 1;
  private slidesLength: number = 0;
  private galleryWrapper: any;

  constructor(id: string, options:IGalleryOptions) {
    this.galleryID = id;
    this.options = {
      speed: options.speed || 3000,
      autoPlay: options.autoPlay || false
    };
    this.init();
  }

  /**
   * Public method for update gallery width.
   */
  public update() {
    this.totalWidth = helpers.getSlideWidth(this.galleryWrapper.childNodes[1]);
    helpers.translateNode(this.galleryWrapper, this.totalWidth * -1, 0);
  }

  /**
   * Public method for activate next slide.
   */
  public nextSlide() {
    let activeSlide = helpers.getChildNodeByClassName(this.galleryWrapper, 'active-slide');
    let nextSlide = activeSlide.nextSibling;

    this.totalWidth += nextSlide.clientWidth;

    if (this.currentSlideValue >= this.slidesLength) {
      this.currentSlideValue = 1;
    } else {
      this.currentSlideValue += 1;
    }

    this.currentSlide.innerHTML = this.currentSlideValue;

    if (nextSlide.classList.contains('first-slide-clone')) {
      helpers.removeClass(nextSlide, 'active-slide');
      helpers.addClassToChild(this.galleryWrapper.childNodes, 1, 'active-slide');
      setTimeout(() => {
        this.totalWidth = nextSlide.clientWidth;
        helpers.translateNode(this.galleryWrapper, this.totalWidth * -1, 0);
      }, 100);
    }  else {
      helpers.addClass(nextSlide, 'active-slide');
    }

    helpers.removeClass(activeSlide, 'active-slide');
    helpers.translateNode(this.galleryWrapper, this.totalWidth * -1);
  }

  /**
   * Public method for activate previous slide.
   */
  public prevSlide() {
    let activeSlide = helpers.getChildNodeByClassName(this.galleryWrapper, 'active-slide');
    let prevSlide = activeSlide.previousSibling;

    this.totalWidth -= prevSlide.clientWidth;

    if (this.currentSlideValue <= 1) {
      this.currentSlideValue = this.slidesLength;
    } else {
      this.currentSlideValue -= 1;
    }

    this.currentSlide.innerHTML = this.currentSlideValue;

    if (prevSlide.classList.contains('last-slide-clone')) {
      helpers.removeClass(prevSlide, 'active-slide');
      helpers.addClassToChild(this.galleryWrapper.childNodes, this.galleryWrapper.childNodes.length - 2, 'active-slide');
      setTimeout(() => {
        this.totalWidth = this.getAllSlidesWidth();
        helpers.translateNode(this.galleryWrapper, this.totalWidth * -1, 0);
      }, 100);
    } else {
      helpers.addClass(prevSlide, 'active-slide');
    }

    helpers.removeClass(activeSlide, 'active-slide');
    helpers.translateNode(this.galleryWrapper, this.totalWidth * -1);
  }

  /**
   * Private method for init gallery.
   */
  private init() {
    this.galleryWrapper = document.getElementById(this.galleryID).childNodes[1];

    if (this.galleryWrapper.attributes[1].value.includes('gallery-wrapper')) {
      helpers.addClassToChild(this.galleryWrapper.childNodes, 0, 'active-slide');

      this.setCounter();
      this.cloneSlides();
      this.bindEvents();
      this.setAutoPlay(this.options.autoPlay, this.options.speed);
      this.setFirstSlide();
    } else {
      console.log(new Error('Wrong gallery structure'));
    }

  }

  /**
   * Private method for bind click event for buttons.
   */
  private bindEvents() {
    let gallery = document.getElementById(this.galleryID);
    let prevButton = helpers.getChildNodeByClassName(gallery, 'gallery-buttons__prev');
    let nextButton = helpers.getChildNodeByClassName(gallery, 'gallery-buttons__next');
    let swipes = new Swipe(gallery);

    swipes.onRight(() => {
      this.prevSlide();
    });

    swipes.onLeft(() => {
      this.nextSlide();
    });

    prevButton.onclick = () => {
      this.prevSlide();
    };

    nextButton.onclick = () => {
      this.nextSlide();
    };

    swipes.run();

    window.addEventListener('resize', () => {
      this.update();
    }, false);
    
  }

  /**
   * Private method to set counter.
   */
  private setCounter() {
    let gallery = document.getElementById(this.galleryID);
    let galleryCounter = helpers.getChildNodeByClassName(gallery, 'counter');
    let all = helpers.getChildNodeByClassName(galleryCounter, 'all');

    this.currentSlide = helpers.getChildNodeByClassName(galleryCounter, 'current');
    this.slidesLength = this.galleryWrapper.childNodes.length;
    this.currentSlide.innerHTML = this.currentSlideValue;
    all.innerHTML = this.slidesLength;
  }

  /**
   * Private method for initialize first slide.
   */
  private setFirstSlide() {
    helpers.slideOnLoad(this.galleryWrapper.childNodes[1], () => {
      this.totalWidth = helpers.getSlideWidth(this.galleryWrapper.childNodes[1]);
      helpers.translateNode(this.galleryWrapper, this.totalWidth * -1, 0);
    });
  }

  /**
   * Private method for get sum of all slides width.
   *
   * @returns {number} - sum of all slides width.
   */
  private getAllSlidesWidth() {
    let width = 0;

    for (let i = 1; i < this.galleryWrapper.childNodes.length - 1; i++) {
      width += helpers.getSlideWidth(this.galleryWrapper.childNodes[i]);
    }

    return width;
  }

  /**
   * Private method for clone slides for loop slide.
   */
  private cloneSlides() {
    let firstSlide = this.galleryWrapper.firstChild.cloneNode(true);
    let lastSlide = this.galleryWrapper.lastChild.cloneNode(true);

    helpers.addClass(firstSlide, 'first-slide-clone');
    helpers.removeClass(firstSlide, 'active-slide');
    helpers.addClass(lastSlide, 'last-slide-clone');

    this.galleryWrapper.appendChild(firstSlide);
    this.galleryWrapper.insertBefore(lastSlide, this.galleryWrapper.firstChild);
  }

  /**
   * Private method for enable gallery autoplay.
   *
   * @param autoPlay:boolean - true for enable.
   * @param time:number - time delay for change slide.
   */
  private setAutoPlay(autoPlay:boolean, time: number) {
    if (autoPlay) {
      setInterval(() => {
        this.nextSlide();
      }, time);
    }
  }
}