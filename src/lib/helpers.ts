export class Helpers {
  constructor() {

  }

  /**
   * Public method for generate images.
   * @param parent:String - id of node where should images placed.
   * @param className?:String - add special class to image, be default 'gallery-slide';
   * @param count?:Number - quantity of generated images, by default 10;
   */
  public generateImages(parent: string, className: string = 'gallery-slide', count: number = 10) {
    let baseImageUrl = 'http://via.placeholder.com/600x350?text=';

    if (parent) {
      let block = document.getElementById(parent);

      for (let i = 1; i < count + 1; i++) {
        let slide = document.createElement('img');
        slide.alt = ' ';
        slide.src = baseImageUrl + i;
        slide.classList.add(className);
        block.appendChild(slide);
      }
    } else {
      console.log(new Error('Parent argument is required'));
    }
  }

  /**
   * Public method for add class for child nodes.
   *
   * @param nodes - nodes list.
   * @param index: number - index of node where should class added.
   * @param className: string - class name to add.
   */
  public addClassToChild(nodes: any, index: number, className: string) {
    nodes[index].classList.add(className);
  }

  /**
   * Public method for get child node by class name.
   *
   * @param node:Node - node for search child.
   * @param className: string - class name of searched node.
   * @returns Node:searched node.
   */
  public getChildNodeByClassName(node: any, className: string) {
    for (let i = 0; i < node.childNodes.length; i++) {
      if (node.childNodes[i].nodeType === 1 && node.childNodes[i].classList.contains(className)) {
        return node.childNodes[i];
      }
    }
  }

  /**
   * Public method for translate3d node.

   * @param node:Node - target node.
   * @param width:number - translate width.
   * @param animation:number - animation time in ms, default 300.
   */
  public translateNode(node: any, width: number, animation: number = 300) {
    node.style.transitionDuration = `${animation}ms`;
    node.style.transform = `translate3d(${width}px, 0px, 0px)`;
    setTimeout(() => {
      node.style.transitionDuration = `0ms`;
    }, animation);
  }

  /**
   * Public method for add class to node.
   *
   * @param node:any - target node.
   * @param className:string - class name.
   */
  public addClass(node: any, className: string) {
    node.classList.add(className);
  }

  /**
   * Public method for remove class from node.
   *
   * @param node:any - target node.
   * @param className:string - class name.
   */
  public removeClass(node: any, className: string) {
    node.classList.remove(className);
  }

  /**
   * Public method for call function on slide load.
   *
   * @param slide
   * @param cb - function which will called after slide load.
   */
  public slideOnLoad(slide: any, cb: any) {
    slide.addEventListener('load', cb, false);
  }

  /**
   * Public method for get slide width.
   *
   * @param slide
   * @returns {number} - slide width.
   */
  public getSlideWidth(slide: any) {
    return slide.clientWidth;
  }
}