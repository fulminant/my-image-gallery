import { Gallery } from './lib/gallery';
import { Helpers } from './lib/helpers';

let helpers = new Helpers();

helpers.generateImages('galleryWrapper');

new Gallery('myCustomGallery', {});